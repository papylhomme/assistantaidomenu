package org.papylhomme.aidomenu;



/**
 * A class representing an Aidomenu product
 */
public class Product {


    /**
     * The product name
     */
    private String name;


    /**
     * The product group
     */
    private String group;


    /**
     * The product brand
     */
    private String brand;



    /**
     * Constructor
     *  @param name The product's name
     * @param group The product's group
     * @param brand The product's brand
     */
    public Product(String name, String group, String brand) {
        this.name = name;
        this.brand = brand;
        this.group = group;
    }



    /**
     * Product's name getter
     *
     * @return The product's name
     */
    public String getName() {
        return name;
    }



    /**
     * Product's brand getter
     *
     * @return The product's brand
     */
    public String getBrand() {
        return brand;
    }



    /**
     * Product's group getter
     *
     * @return The product's group
     */
    public String getGroup() {
        return group;
    }



    /**
     * Override to display product information
     *
     * @return A string representation of the product
     */
    @Override
    public String toString() {
        return this.name + "(" + this.brand + "/" + this.group + ")";
    }

}
