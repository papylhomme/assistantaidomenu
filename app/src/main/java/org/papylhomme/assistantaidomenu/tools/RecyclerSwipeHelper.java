package org.papylhomme.assistantaidomenu.tools;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import org.papylhomme.assistantaidomenu.R;



/**
 * Helper for recycler view swiping support
 */
public class RecyclerSwipeHelper extends ItemTouchHelper.SimpleCallback {

    /**
     * Internal reference to the recycler view adapter
     */
    private RecyclerSwipeAdapter adapter;


    /**
     * An icon for the background
     */
    private Drawable icon;


    /**
     * Background color behind the swiping card
     */
    private ColorDrawable background;



    /**
     * Static helper to setup the recycler view for swipe to remove. The adapter must be set as it is used.
     *
     * @param ctx A context to retrieve resources
     * @param recyclerView The recycler view to setup
     * @return An ItemTouchHelper instance
     */
    public static ItemTouchHelper setupRecyclerView(Context ctx, RecyclerView recyclerView) {
        return setupRecyclerView(ctx, recyclerView, R.drawable.icon_stock_update_light, R.color.colorAccent);
    }



    /**
     * Static helper to setup the recycler view for swipe to remove. The adapter must be set as it is used.
     *
     * @param ctx A context to retrieve resources
     * @param recyclerView The recycler view to setup
     * @return An ItemTouchHelper instance
     */
    public static ItemTouchHelper setupRecyclerView(Context ctx, RecyclerView recyclerView, int swipeIconRes, int swipeBackgroundColorRes) {
        return new ItemTouchHelper(new RecyclerSwipeHelper(ctx, recyclerView.getAdapter(), swipeIconRes, swipeBackgroundColorRes));
    }



    /**
     * Create a new swipe remove helper
     *
     * @param ctx A context to retrieve resources
     * @param adapter The adapter
     */
    public RecyclerSwipeHelper(Context ctx, RecyclerView.Adapter adapter, int swipeIconRes, int swipeBackgroundColorRes) {
        super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);

        this.background = new ColorDrawable(ctx.getResources().getColor(swipeBackgroundColorRes));
        this.background.setAlpha(0);

        this.icon = ctx.getResources().getDrawable(swipeIconRes);

        //only assign adapter if it implements RecyclerSwipeAdapter interface
        if(adapter instanceof RecyclerSwipeAdapter)
            this.adapter = (RecyclerSwipeAdapter) adapter;
    }



    /**
     * Disable card moving
     *
     * @param recyclerView Unused
     * @param viewHolder Unused
     * @param target Unused
     * @return false
     */
    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }



    /**
     * Enable swipe only for removable items
     *
     * @param recyclerView Instance of recycler view
     * @param viewHolder The view holder
     * @return 0 if there is no swipe remove adapter or the item is not removable, swipe direction otherwise
     */
    @Override
    public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        //disable swiping if no swipe remove adapter present
        if(adapter == null)
            return 0;

        if(adapter.isItemSwipeable(viewHolder.getAdapterPosition()))
            return super.getSwipeDirs(recyclerView, viewHolder);
        else
            return 0;
    }



    /**
     * Handle card swiped by calling the adapter to remove the item
     *
     * @param viewHolder Reference to the swiped view holder
     * @param direction Unused
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        //safety check, should not happen
        if(adapter == null)
            return;

        int pos = viewHolder.getAdapterPosition();

        if(adapter.isItemSwipeable(pos)) {
            adapter.onSwipeItem(pos);
        }
    }



    /**
     * Draw a background behind the swiped row
     *
     * @param c The canvas to draw into
     * @param recyclerView The recycler view
     * @param viewHolder The current view holder
     * @param dX X translation
     * @param dY Y translation
     * @param actionState current state
     * @param isCurrentlyActive Unused
     */
    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        //skip if no adapter
        if(adapter == null)
            return;

        //fix limits to the swipe
        if(dX > 255)
            dX = 255;
        else if(dX < -255)
            dX = -255;

        //skip drawing when their is no swipe
        if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE && (dX > 0 || dX < 0)) {
            int left = 0;
            int right = 0;
            int alpha = 0;
            View item = viewHolder.itemView;

            if(dX > 0) {
                left = (int) (item.getLeft() + (dX - icon.getIntrinsicWidth()) / 2);
                right = left + icon.getIntrinsicWidth();
            } else if(dX < 0) {
                right = (int) (item.getRight() + (dX + icon.getIntrinsicWidth()) / 2);
                left = right - icon.getIntrinsicWidth();
            }

            alpha = (int) (alpha + Math.abs(dX));
            if(alpha > 255)
                alpha = 255;

            //draw background color
            background.setBounds(item.getLeft(), item.getTop(), item.getRight(), item.getBottom());
            background.setAlpha(alpha);
            background.draw(c);

            //compute position and draw icon
            int height = item.getBottom() - item.getTop();
            int top = item.getTop() + height / 2 - icon.getIntrinsicHeight() / 2;
            int bottom = top + icon.getIntrinsicHeight();

            icon.setBounds(left, top, right, bottom);
            icon.draw(c);
        }

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }



    /**
     * Interface to implement for item removal behaviour
     */
    public interface RecyclerSwipeAdapter {

        /**
         * Test if the item at position is swipeable
         *
         * @param position Position of the item
         * @return True if the item is swipeable, false otherwise
         */
        boolean isItemSwipeable(int position);


        /**
         * Swipe the item at position
         *
         * @param position Position of the item
         */
        void onSwipeItem(int position);

    }
}