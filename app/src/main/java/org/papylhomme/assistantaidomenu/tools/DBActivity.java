package org.papylhomme.assistantaidomenu.tools;

import android.os.Bundle;

import org.papylhomme.assistantaidomenu.db.DBHelper;



/**
 * Base class for activities using the database.
 */
public abstract class DBActivity extends DrawerActivity {

    /**
     * A DBHelper instance
     */
    protected DBHelper dbHelper;



    /**
     * Overridden to instantiate a DBHelper
     *
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHelper = new DBHelper(this);
    }



    /**
     * Overridden to close db connections
     */
    @Override
    protected void onDestroy() {
        dbHelper.close();
        super.onDestroy();
    }
}
