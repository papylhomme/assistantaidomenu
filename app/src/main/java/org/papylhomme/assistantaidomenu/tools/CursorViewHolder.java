package org.papylhomme.assistantaidomenu.tools;

import android.support.v7.widget.RecyclerView;
import android.view.View;



/**
 * Base class for view holder using CursorRecyclerAdapter
 */
public abstract class CursorViewHolder<Item> extends RecyclerView.ViewHolder {


    /**
     * Create a new view holder for the given item view
     *
     * @param itemView A view instance
     */
    public CursorViewHolder(View itemView) {
        super(itemView);
    }



    /**
     * Bind the view to the given item
     *
     * @param item An item instance
     */
    public abstract void bind(Item item);
}
