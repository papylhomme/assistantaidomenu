package org.papylhomme.assistantaidomenu.tools;


import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.ui.ActivityHome;
import org.papylhomme.assistantaidomenu.ui.products.ActivityProducts;
import org.papylhomme.assistantaidomenu.ui.stock.ActivityStock;


/**
 * Base class for activities using the drawer
 */
public abstract class DrawerActivity extends CoordinatorLayoutActivity {

    /**
     * Reference to the drawer toggle in the action bar
     */
    private ActionBarDrawerToggle drawerToggle;


    /**
     * Internal reference to the drawer layout
     */
    private DrawerLayout drawerLayout;



    /**
     * Overridden to initialize the toolbar and the drawer
     *
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setup toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        toolbar.setSubtitle(getSubtitle());

        //init drawer
        initDrawer();

        //set the content view
        ViewStub stub = (ViewStub) findViewById(R.id.content_view);
        stub.setLayoutResource(getContentView());
        stub.inflate();

        //query the loader manager instance here to avoid the following bug
        //http://stackoverflow.com/questions/10170481/loader-can-not-be-restarted-after-orientation-changed
        getLoaderManager();
    }



    /**
     * Callback used to retrieve an context layout
     *
     * @return A layout resource
     */
    protected abstract int getContentView();



    /**
     * Retrieve a subtitle for this context
     *
     * @return A string or null
     */
    protected String getSubtitle() {
        return null;
    }



    /**
     * Update the action bar title
     *
     * @param title The title
     */
    protected void updateTitle(String title) {
        getSupportActionBar().setTitle(title);
    }



    /**
     * Update the action bar subtitle
     *
     * @param subtitle The subtitle
     */
    protected void updateSubtitle(String subtitle) {
        getSupportActionBar().setSubtitle(subtitle);
    }



    /**
     * Initialize the drawer
     */
    private void initDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        setupActionBar();

        setShortcutListener(findViewById(R.id.button_home), ActivityHome.class);
        setShortcutListener(findViewById(R.id.button_products), ActivityProducts.class);
        setShortcutListener(findViewById(R.id.button_stock), ActivityStock.class);
    }



    /**
     * Setup the action bar for drawer integration
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();

        //skip method if no action bar is present
        if(actionBar == null)
            return;

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

        };

        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(drawerToggle);
    }



    /**
     * Overridden to handle drawer toggle click
     *
     * @param item The menu item
     * @return Boolean indicated if the click was handled or not
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }



    /**
     * Overridden to sync the drawer state
     *
     * @param savedInstanceState Unused by this implementation
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }



    /**
     * Overridden to sync the drawer state
     *
     * @param newConfig Unused by this implementation
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }



    /**
     * Helper to set a shortcut listener for buttons
     *
     * @param view The view responsible to handle the click
     * @param aClass The context class to start
     */
    protected void setShortcutListener(View view, final Class aClass) {
        setShortcutListener(view, aClass, Bundle.EMPTY);
    }



    /**
     * Helper to set a shortcut listener for buttons
     *
     * @param view The view responsible to handle the click
     * @param aClass The context class to start
     * @param params A bundle of parameters
     */
    protected void setShortcutListener(View view, final Class aClass, final Bundle params) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                Intent intent = new Intent(DrawerActivity.this, aClass);
                intent.putExtras(params);

                startActivityIfNeeded(intent, 0);
            }
        });

    }

}
