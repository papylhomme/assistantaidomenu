package org.papylhomme.assistantaidomenu.tools;

import android.app.Fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.futuremind.recyclerviewfastscroll.FastScroller;

import org.papylhomme.assistantaidomenu.R;


/**
 * Base class for fragment displaying a recycler view populated from a cursor
 */
public abstract class RecyclerListFragment<Item, ViewHolder extends CursorViewHolder<Item>>
        extends Fragment
        implements CursorListFragment {


    /**
     * The instance of adapter for the list
     */
    protected CursorRecyclerAdapter<Item, ViewHolder> adapter;


    /**
     * The recycler view instance
     */
    protected RecyclerView list;


    /**
     * The fast scroller view instance
     */
    protected FastScroller fastScroller;


    /**
     * The item count label instance
     */
    private TextView itemCountLabel;



    /**
     * Get the layout resource to inflate for the fragment
     *
     * @return A layout resource
     */
    abstract protected int getLayout();



    /**
     * Get the id of the recycler view in the fragment's layout
     *
     * @return An id for the recycler view
     */
    abstract protected int getRecyclerView();



    /**
     * Create an instance of adapter for the recycler view
     *
     * @return An adapter instance
     */
    abstract protected CursorRecyclerAdapter<Item, ViewHolder> createListAdapter();



    /**
     * Get the id of the fast scroller for the recycler view
     *
     * @return An id for the fast scroller view
     */
    protected int getFastScroll() {
        return R.id.fastscroll;
    }



    /**
     * Handle onCreate by initializing the list adapter
     *
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = createListAdapter();
    }



    /**
     * Handle onCreateView by inflating the layout and initializing the recycler view and fast scroller
     *
     * @param inflater A layout inflater instance
     * @param container A view container instance
     * @param savedInstanceState A saved instance
     * @return The layout view
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =  inflater.inflate(getLayout(), container, false);

        list = (RecyclerView) rootView.findViewById(getRecyclerView());
        list.setAdapter(adapter);

        fastScroller = (FastScroller) rootView.findViewById(getFastScroll());
        fastScroller.setRecyclerView(list);

        itemCountLabel = (TextView) rootView.findViewById(R.id.label_items_count);

        return rootView;
    }



    /**
     * Handle onResume by updating the item count label
     */
    @Override
    public void onResume() {
        super.onResume();
        updateItemCountLabel();
    }



    /**
     * Update the list of items with the given cursor
     *
     * @param cursor A cursor wrapping the items
     */
    public void updateItems(Cursor cursor) {
        adapter.setCursor(cursor);
        updateItemCountLabel();
    }



    /**
     * Update the item count label
     */
    private void updateItemCountLabel() {
        itemCountLabel.setText(getString(R.string.label_items_count, adapter.getItemCount()));
    }

}
