package org.papylhomme.assistantaidomenu.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;

import org.papylhomme.assistantaidomenu.db.DBHelper;

/**
 * Import service
 */
public class ImportService extends IntentService {


    /**
     * Name of the parameter for the URI
     */
    public static final String PARAM_URI = "IMPORT_URI";



    /**
     * Import constants
     */
    public static final String ACTION_IMPORT_PRODUCTS = "org.papylhomme.assistantaidomenu.action.IMPORT_PRODUCTS";
    public static final String ACTION_IMPORT_STOCK = "org.papylhomme.assistantaidomenu.action.IMPORT_STOCK";



    /**
     * Event constants
     */
    public static final String INTENT_ACTION_EVENT = "android.intent.action.IMPORT_EVENT";
    public static final String PARAM_ORIGINAL_ACTION = "PARAM_ORIGINAL_ACTION";
    public static final String PARAM_EVENT_TYPE = "PARAM_EVENT_TYPE";
    public static final String PARAM_IMPORT_PROGRESS = "PARAM_IMPORT_PROGRESS";
    public static final String PARAM_IMPORT_COUNT = "PARAM_IMPORT_COUNT";
    public static final String PARAM_ERROR_MESSAGE = "PARAM_ERROR_MESSAGE";

    public enum EVENT_TYPE {
        IMPORT_STARTED,
        IMPORT_PROGRESS,
        IMPORT_FINISHED,
        IMPORT_ERROR
    }



    /**
     * A DBHelper instance to access the database
     */
    private DBHelper dbHelper;



    /**
     * Constructor
     */
    public ImportService() {
        super("ImportService");
    }



    /**
     * Helper to start an import products request
     *
     * @param context Caller context
     */
    public static void importProducts(Context context, Uri uri) {
        Intent intent = new Intent(context, ImportService.class);
        intent.setAction(ACTION_IMPORT_PRODUCTS);
        intent.putExtra(PARAM_URI, uri);
        context.startService(intent);
    }



    /**
     * Helper to start an import stock request
     *
     * @param context Caller context
     */
    public static void importStock(Context context, Uri uri) {
        Intent intent = new Intent(context, ImportService.class);
        intent.setAction(ACTION_IMPORT_STOCK);
        intent.putExtra(PARAM_URI, uri);
        context.startService(intent);
    }



    /**
     *
     */
    @Override
    public void onCreate() {
        super.onCreate();
        this.dbHelper = new DBHelper(this);
    }



    /**
     * Handle service intents
     *
     * @param intent The intent to handle
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();

            if (ACTION_IMPORT_PRODUCTS.equals(action)) {
                handleImportProducts((Uri) intent.getParcelableExtra(PARAM_URI));

            } else if (ACTION_IMPORT_STOCK.equals(action)) {
                handleImportStock((Uri) intent.getParcelableExtra(PARAM_URI));
            }
        }
    }



    /**
     * Start an import products task
     */
    private void handleImportProducts(Uri uri) {
        new ImportProductsTask(this, dbHelper.getWritableDatabase()).execute(uri);
    }



    /**
     * Start an import stock task
     */
    private void handleImportStock(Uri uri) {
        new ImportStockTask(this, dbHelper.getWritableDatabase()).execute(uri);
    }
}
