package org.papylhomme.assistantaidomenu.provider.requests;

import android.content.Context;
import android.content.CursorLoader;
import android.net.Uri;

import org.papylhomme.assistantaidomenu.db.DBContract.Products;
import org.papylhomme.assistantaidomenu.db.DBHelper;
import org.papylhomme.assistantaidomenu.db.Query;

import java.util.ArrayList;

import static org.papylhomme.assistantaidomenu.provider.DataProvider.AUTHORITY;



/**
 * Request the product's list
 */
public class ListProductRequest extends BaseProductsRequest {


    /**
     * Constructor
     *
     * @param context A context instance
     * @param dbHelper A DBHelper instance
     */
    public ListProductRequest(Context context, DBHelper dbHelper) {
        super(context, dbHelper);

        addUri(Products.TABLE_NAME, 0);
    }



    /**
     * Get a mime type for the request
     *
     * @param uri A request uri
     * @return A mime type
     */
    @Override
    public String getType(Uri uri) {
        return "vnd.android.cursor.dir/" + AUTHORITY + "/" + Products.TABLE_NAME;
    }



    /**
     * Create a products loader
     *
     * @param context A context instance
     * @param searchQuery An optional search query
     * @param groupFilter An optional group filter
     * @return A loader instance
     */
    public static CursorLoader list(Context context, String searchQuery, ArrayList<Integer> groupFilter) {
        String sort = Query.formatSort("ASC", Query.fqcn(Products.TABLE_NAME, Products.COLUMN_NAME));
        Query.Selection selection = new Query.Selection()
                .glob(Query.fqcn(Products.TABLE_NAME, Products.COLUMN_NORMALIZED_NAME), searchQuery)
                .in(Query.fqcn(Products.TABLE_NAME, Products.COLUMN_GROUP_ID), groupFilter);

        return createLoader(URI, context, getDefaultProjection(), sort, selection);
    }
}
