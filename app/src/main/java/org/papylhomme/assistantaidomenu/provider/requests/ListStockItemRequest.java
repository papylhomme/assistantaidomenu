package org.papylhomme.assistantaidomenu.provider.requests;

import android.content.Context;
import android.content.CursorLoader;
import android.net.Uri;

import org.papylhomme.assistantaidomenu.db.DBContract.Stock;
import org.papylhomme.assistantaidomenu.db.DBHelper;
import org.papylhomme.assistantaidomenu.db.Query;

import static org.papylhomme.assistantaidomenu.provider.DataProvider.AUTHORITY;



/**
 * Request the stock list
 */
public class ListStockItemRequest extends BaseStockItemsRequest {


    /**
     * Constructor
     *
     * @param context A context instance
     * @param dbHelper A DBHelper instance
     */
    public ListStockItemRequest(Context context, DBHelper dbHelper) {
        super(context, dbHelper);

        addUri(Stock.TABLE_NAME, 0);
    }



    /**
     * Get a mime type for the request
     *
     * @param uri A request uri
     * @return A mime type
     */
    @Override
    public String getType(Uri uri) {
        return "vnd.android.cursor.dir/" + AUTHORITY + "/" + Stock.TABLE_NAME;
    }



    /**
     * Create a stock item loader
     *
     * @param context A context instance
     * @param selection A selection for the request
     * @return A loader instance
     */
    public static CursorLoader list(Context context, Query.Selection selection) {
        return list(context, selection, getDefaultSort());
    }


    /**
     * Create a stock item loader
     *
     * @param context A context instance
     * @param selection A selection for the request
     * @param sort A sort order clause
     * @return A loader instance
     */
    public static CursorLoader list(Context context, Query.Selection selection, String sort) {
        return createLoader(URI, context, getDefaultProjection(), sort, selection);
    }

}
