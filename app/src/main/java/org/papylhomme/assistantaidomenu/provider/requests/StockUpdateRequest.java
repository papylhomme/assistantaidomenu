package org.papylhomme.assistantaidomenu.provider.requests;

import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import org.papylhomme.assistantaidomenu.provider.ContentRequestHandler;
import org.papylhomme.assistantaidomenu.db.DBContract.StockUpdate;
import org.papylhomme.assistantaidomenu.db.DBContract.Stock;
import org.papylhomme.assistantaidomenu.db.DBHelper;

import static org.papylhomme.assistantaidomenu.provider.DataProvider.AUTHORITY;



/**
 * Request the stock updates
 */
public class StockUpdateRequest extends ContentRequestHandler {


    /**
     * The Uri for stock items update
     */
    public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + StockUpdate.TABLE_NAME);



    /**
     * Constructor
     *
     * @param context A context instance
     * @param dbHelper A DBHelper instance
     */
    public StockUpdateRequest(Context context, DBHelper dbHelper) {
        super(context, dbHelper);

        addUri(StockUpdate.TABLE_NAME, 0);
    }



    /**
     * Get a mime type for the request
     *
     * @param uri A request uri
     * @return A mime type
     */
    @Override
    public String getType(Uri uri) {
        switch(uriMatcher.match(uri)) {
            case 0: return "vnd.android.cursor.dir/" + AUTHORITY + "/" + StockUpdate.TABLE_NAME;
            default: return null;
        }
    }



    /**
     * Query the stock's list
     *
     * @param uri A request uri
     * @param projection Projection for the query
     * @param selection Selection for the query
     * @param args Arguments for selection
     * @param sortOrder A sort order
     * @return A cursor wrapping the results
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] args, String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        return db.query(StockUpdate.TABLE_NAME, projection, selection, args, null, null, sortOrder);
    }



    /**
     * Insert a new stock update and update the current stock amount in the related stock item
     *
     * @param uri A request uri
     * @param contentValues Values to insert
     * @return The number of inserted rows
     */
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        long stockItemId = contentValues.getAsLong("stockItemId");
        double amount = contentValues.getAsDouble("amount");
        String type = contentValues.getAsString("type");
        String operation = contentValues.getAsString("operation");

        String table = Stock.TABLE_NAME;
        String itemSelection = Stock._ID + " = " + stockItemId;

        try {
            db.beginTransaction();

            //fetch current quantity
            Cursor c = db.query(table, null, itemSelection, null, null, null, null);
            c.moveToFirst();
            double current = c.getDouble(c.getColumnIndex(Stock.COLUMN_CURRENT));
            c.close();

            //update current quantity
            ContentValues stockValues = new ContentValues();
            if ("-".equals(operation)) {
                stockValues.put(Stock.COLUMN_CURRENT, current - amount);
                amount = amount * -1;

            } else if ("+".equals(operation)) {
                stockValues.put(Stock.COLUMN_CURRENT, current + amount);

            } else if ("=".equals(operation)) {
                stockValues.put(Stock.COLUMN_CURRENT, amount);
                amount = amount - current;
            }

            db.update(table, stockValues, itemSelection, null);


            //add stock update
            ContentValues stockUpdateValues = new ContentValues();
            stockUpdateValues.put(StockUpdate.COLUMN_STOCK_ITEM_ID, stockItemId);
            stockUpdateValues.put(StockUpdate.COLUMN_TYPE, type);
            stockUpdateValues.put(StockUpdate.COLUMN_AMOUNT, amount);
            stockUpdateValues.put(StockUpdate.COLUMN_TIMESTAMP, System.currentTimeMillis());
            long stockUpdateId = db.insert(StockUpdate.TABLE_NAME, null, stockUpdateValues);

            db.setTransactionSuccessful();

            //notify
            notifyChange(StockUpdateRequest.URI);
            notifyChange(Uri.withAppendedPath(GetStockItemRequest.URI, String.valueOf(stockItemId)));
            notifyChange(Uri.withAppendedPath(ListUpdatedStockItemRequest.URI, String.valueOf(stockItemId)));

            return Uri.withAppendedPath(URI, String.valueOf(stockUpdateId));

        } catch(Exception ex) {
            Log.e(StockUpdateRequest.class.getName(), "Error inserting new stock update", ex);
            return null;

        } finally {
            db.endTransaction();
        }
    }



    /**
     * Delete the stock update at the given uri and update the related stock item accordingly
     *
     * @param uri A request uri
     * @param selection Selection for the request
     * @param args Arguments for selection
     * @return The number of deleted rows
     */
    @Override
    public int delete(Uri uri, String selection, String[] args) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        try {
            db.beginTransaction();

            //retrieve stock update information
            Cursor c = db.query(StockUpdate.TABLE_NAME, null, selection, args, null, null, null);
            c.moveToFirst();
            double amount = c.getDouble(c.getColumnIndex(StockUpdate.COLUMN_AMOUNT));
            long stockItemId = c.getLong(c.getColumnIndex(StockUpdate.COLUMN_STOCK_ITEM_ID));
            c.close();

            //retrieve stock item information
            String stockItemSelection = Stock._ID + " = ?";
            String[] stockItemSelectionArgs = new String[]{String.valueOf(stockItemId)};
            c = db.query(Stock.TABLE_NAME, null, stockItemSelection, stockItemSelectionArgs, null, null, null);
            c.moveToFirst();
            double current = c.getDouble(c.getColumnIndex(Stock.COLUMN_CURRENT));
            c.close();

            //update stock item current value
            ContentValues updatedStockItemValues = new ContentValues();
            updatedStockItemValues.put(Stock.COLUMN_CURRENT, current - amount);
            db.update(Stock.TABLE_NAME, updatedStockItemValues, stockItemSelection, stockItemSelectionArgs);

            int count = db.delete(StockUpdate.TABLE_NAME, selection, args);

            db.setTransactionSuccessful();

            //notify
            notifyChange(StockUpdateRequest.URI);
            notifyChange(Uri.withAppendedPath(GetStockItemRequest.URI, String.valueOf(stockItemId)));
            notifyChange(Uri.withAppendedPath(ListUpdatedStockItemRequest.URI, String.valueOf(stockItemId)));

            return count;

        } catch(Exception ex) {
            Log.e(StockUpdateRequest.class.getName(), "Error inserting new stock update", ex);
            return 0;

        } finally {
            db.endTransaction();
        }
    }



    /**
     * Create a loader to fetch a list of stock updates
     *
     * @param context A context instance
     * @param stockItemId The id of the stock item to load
     * @return A loader instance
     */
    public static CursorLoader list(Context context, long stockItemId) {
        String selection = StockUpdate.COLUMN_STOCK_ITEM_ID + " = ?";
        String[] args = new String[] {String.valueOf(stockItemId)};

        return new CursorLoader(context, URI, null, selection, args, StockUpdate.COLUMN_TIMESTAMP + " ASC");
    }



    /**
     * Update a stock item
     *
     * @param context A context instance
     * @param stockItemId Id of the stock item to update
     * @param amount The amount to apply to operation
     * @param type Type of stock update (label)
     * @param operation The type of operation (-, +, =)
     */
    public static Uri newStockUpdate(Context context, long stockItemId, double amount, String type, String operation) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("stockItemId", stockItemId);
        contentValues.put("amount", amount);
        contentValues.put("type", type);
        contentValues.put("operation", operation);

        return context.getContentResolver().insert(URI, contentValues);
    }



    /**
     * Remove the given stock update
     *
     * @param context A context instance
     * @param stockUpdateId Id of the stock update to delete
     * @return The number of removed rows
     */
    public static int removeStockUpdate(Context context, long stockUpdateId) {
        String selection = StockUpdate._ID + " = ?";
        String[] args = new String[] {String.valueOf(stockUpdateId)};

        return context.getContentResolver().delete(URI, selection, args);
    }
}
