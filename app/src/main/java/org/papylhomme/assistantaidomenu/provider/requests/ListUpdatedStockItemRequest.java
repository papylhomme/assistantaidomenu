package org.papylhomme.assistantaidomenu.provider.requests;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;

import org.papylhomme.assistantaidomenu.db.DBContract.ProductGroup;
import org.papylhomme.assistantaidomenu.db.DBContract.Stock;
import org.papylhomme.assistantaidomenu.db.DBContract.StockUpdate;
import org.papylhomme.assistantaidomenu.db.DBHelper;
import org.papylhomme.assistantaidomenu.db.Query;

import static org.papylhomme.assistantaidomenu.provider.DataProvider.AUTHORITY;



/**
 * Request the updated stock items list
 */
public class ListUpdatedStockItemRequest extends ListStockItemRequest {


    /**
     * The Uri for updated stock items
     */
    public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + Stock.TABLE_NAME + "/updated");



    /**
     * Constructor
     *
     * @param context A context instance
     * @param dbHelper A DBHelper instance
     */
    public ListUpdatedStockItemRequest(Context context, DBHelper dbHelper) {
        super(context, dbHelper);

        addUri(Stock.TABLE_NAME + "/updated", 0);
    }



    /**
     * Query the updated stock's list
     *
     * @param uri A request uri
     * @param projection Projection for the query
     * @param selection Selection for the query
     * @param args Arguments for selection
     * @param sortOrder A sort order
     * @return A cursor wrapping the results
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] args, String sortOrder) {
        return new Query(Stock.TABLE_NAME, projection)
                .join(Stock.TABLE_NAME, Stock.COLUMN_GROUP_ID, ProductGroup.TABLE_NAME, ProductGroup._ID)
                .join("LEFT JOIN", Stock.TABLE_NAME, Stock._ID, StockUpdate.TABLE_NAME, StockUpdate.COLUMN_STOCK_ITEM_ID)
                .selection(selection, args)
                .group(Query.fqcn(Stock.TABLE_NAME, Stock._ID))
                .having(String.format("COUNT(%s) > 0", Query.fqcn(StockUpdate.TABLE_NAME, StockUpdate._ID)))
                .sort(sortOrder)
                .execute(dbHelper.getReadableDatabase());
    }



    /**
     * Create a stock item loader
     *
     * @param context A context instance
     * @param selection A selection for the request
     * @return A loader instance
     */
    public static CursorLoader list(Context context, Query.Selection selection) {
        return list(context, selection, getDefaultSort());
    }



    /**
     * Create a stock item loader
     *
     * @param context A context instance
     * @param selection A selection for the request
     * @param sort A sort order clause
     * @return A loader instance
     */
    public static CursorLoader list(Context context, Query.Selection selection, String sort) {
        return createLoader(URI, context, getDefaultProjection(), sort, selection);
    }
}
