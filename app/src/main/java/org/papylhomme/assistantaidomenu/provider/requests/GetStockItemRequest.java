package org.papylhomme.assistantaidomenu.provider.requests;

import android.content.Context;
import android.content.CursorLoader;
import android.net.Uri;

import org.papylhomme.assistantaidomenu.db.DBContract.Stock;
import org.papylhomme.assistantaidomenu.db.DBHelper;
import org.papylhomme.assistantaidomenu.db.Query;

import static org.papylhomme.assistantaidomenu.provider.DataProvider.AUTHORITY;



/**
 * Request a stock item
 */
public class GetStockItemRequest extends BaseStockItemsRequest {


    /**
     * Constructor
     *
     * @param context A context instance
     * @param dbHelper A DBHelper instance
     */
    public GetStockItemRequest(Context context, DBHelper dbHelper) {
        super(context, dbHelper);

        addUri(Stock.TABLE_NAME + "/#", 0);
    }



    /**
     * Get a mime type for the request
     *
     * @param uri A request uri
     * @return A mime type
     */
    @Override
    public String getType(Uri uri) {
        return "vnd.android.cursor.item/" + AUTHORITY + "/" + Stock.TABLE_NAME;
    }



    /**
     * Create a loader to fetch a product
     *
     * @param context A context instance
     * @param stockItemId The id of the stock item to load
     * @return A loader instance
     */
    public static CursorLoader get(Context context, int stockItemId) {
        String stringId = Integer.toString(stockItemId);
        Query.Selection selection = new Query.Selection().match(Query.fqcn(Stock.TABLE_NAME, Stock._ID), stringId);

        return createLoader(Uri.withAppendedPath(URI, stringId), context, getDefaultProjection(), null, selection);
    }
}
