package org.papylhomme.assistantaidomenu.provider.requests;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import org.papylhomme.assistantaidomenu.db.DBContract.ProductGroup;
import org.papylhomme.assistantaidomenu.db.DBHelper;
import org.papylhomme.assistantaidomenu.provider.ContentRequestHandler;

import static org.papylhomme.assistantaidomenu.provider.DataProvider.AUTHORITY;

/**
 * Request the groups
 */
public class ListProductGroupRequest extends ContentRequestHandler {


    /**
     * The Uri for products
     */
    public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + ProductGroup.TABLE_NAME);




    /**
     * Constructor
     *
     * @param context A context instance
     * @param dbHelper A DBHelper instance
     */
    public ListProductGroupRequest(Context context, DBHelper dbHelper) {
        super(context, dbHelper);

        addUri(ProductGroup.TABLE_NAME, 0);
    }



    /**
     * Get a mime type for the request
     *
     * @param uri A request uri
     * @return A mime type
     */
    @Override
    public String getType(Uri uri) {
        return "vnd.android.cursor.dir/" + AUTHORITY + "/" + ProductGroup.TABLE_NAME;
    }



    /**
     * Query the group's list
     *
     * @param uri A request uri
     * @param projection Projection for the query
     * @param selection Selection for the query
     * @param args Arguments for selection
     * @param sortOrder A sort order
     * @return A cursor wrapping the results
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] args, String sortOrder) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String sql = "SELECT " +
                String.format(" %1$s.%2$s AS %2$s,", ProductGroup.TABLE_NAME, ProductGroup._ID) +
                String.format(" %1$s.%2$s AS %2$s ", ProductGroup.TABLE_NAME, ProductGroup.COLUMN_NAME) +
                " FROM " + ProductGroup.TABLE_NAME;

        if(args != null && args.length == 1) {
            String tableName = args[0];

            sql += " JOIN " + tableName +
                    String.format(" ON %s.%s = %s.%s", ProductGroup.TABLE_NAME, ProductGroup._ID, tableName, "group_id") +
                    String.format(" GROUP BY %s.%s ", ProductGroup.TABLE_NAME, ProductGroup._ID) +
                    String.format(" HAVING COUNT(%s.%s) > 0", tableName, "_id");
        }

        if(sortOrder != null && !sortOrder.isEmpty())
            sql += " ORDER BY " + sortOrder;

        return db.rawQuery(sql, null);
    }



    /**
     * Create a loader to fetch a list of groups
     *
     * @param context A context instance
     * @return A loader instance
     */
    public static CursorLoader createProductGroupListLoader(Context context) {
        return createProductListLoader(context, null);
    }



    /**
     * Create a loader to fetch a list of groups
     *
     * @param context A context instance
     * @return A loader instance
     */
    public static CursorLoader createProductGroupListLoader(Context context, String relationTable) {
        return createProductListLoader(context, new String[]{relationTable});
    }



    /**
     * Create a loader to fetch a list of groups
     *
     * @param context A context instance
     * @param args Arguments for selection
     * @return A loader instance
     */
    private static CursorLoader createProductListLoader(Context context, String[] args) {
        return new CursorLoader(context,
                URI,
                null,
                null,
                args,
                ProductGroup.COLUMN_NAME + " ASC");
    }
}
