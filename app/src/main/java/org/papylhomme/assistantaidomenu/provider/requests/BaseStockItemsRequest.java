package org.papylhomme.assistantaidomenu.provider.requests;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import org.papylhomme.assistantaidomenu.db.DBHelper;
import org.papylhomme.assistantaidomenu.db.Query;
import org.papylhomme.assistantaidomenu.provider.ContentRequestHandler;

import org.papylhomme.assistantaidomenu.db.DBContract.Stock;
import org.papylhomme.assistantaidomenu.db.DBContract.ProductGroup;

import static org.papylhomme.assistantaidomenu.provider.DataProvider.AUTHORITY;


/**
 * Base class for stock items requests
 */
@SuppressWarnings("WeakerAccess")
public abstract class BaseStockItemsRequest  extends ContentRequestHandler {


    /**
     * The Uri for stock items
     */
    public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + Stock.TABLE_NAME);



    /**
     * Constructor
     *
     * @param context  A context instance
     * @param dbHelper A DBHelper instance
     */
    public BaseStockItemsRequest(Context context, DBHelper dbHelper) {
        super(context, dbHelper);
    }



    /**
     * Query a stock item
     *
     * @param uri        A request uri
     * @param projection Projection for the query
     * @param selection  Selection for the query
     * @param args       Arguments for selection
     * @param sortOrder  A sort order
     * @return A cursor wrapping the result
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] args, String sortOrder) {
        return new Query(Stock.TABLE_NAME, projection)
                .join(Stock.TABLE_NAME, Stock.COLUMN_GROUP_ID, ProductGroup.TABLE_NAME, ProductGroup._ID)
                .selection(selection, args)
                .sort(sortOrder)
                .execute(dbHelper.getReadableDatabase());
    }



    /**
     * Get the default projection for stock items
     *
     * @return A projection list
     */
    public static String[] getDefaultProjection() {
        return new String[]{
                Query.fqcn2n(Stock.TABLE_NAME, Stock._ID),
                Query.fqcn2n(Stock.TABLE_NAME, Stock.COLUMN_NAME),
                Query.fqcn2n(ProductGroup.TABLE_NAME, ProductGroup.COLUMN_NAME),
                Query.fqcn2n(Stock.TABLE_NAME, Stock.COLUMN_UNIT),
                Query.fqcn2n(Stock.TABLE_NAME, Stock.COLUMN_BRAND),
                Query.fqcn2n(Stock.TABLE_NAME, Stock.COLUMN_BBD),
                Query.fqcn2n(Stock.TABLE_NAME, Stock.COLUMN_INITIAL),
                Query.fqcn2n(Stock.TABLE_NAME, Stock.COLUMN_CURRENT),
                Query.fqcn2n(Stock.TABLE_NAME, Stock.COLUMN_TIMESTAMP),
        };
    }



    /**
     * Get the default sort clause for stock items
     *
     * @return A sort clause
     */
    public static String getDefaultSort() {
        return Query.formatSort("ASC",
                Query.fqcn(Stock.TABLE_NAME, Stock.COLUMN_NAME),
                Query.fqcn(Stock.TABLE_NAME, Stock.COLUMN_BBD)
        );
    }

}
