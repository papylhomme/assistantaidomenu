package org.papylhomme.assistantaidomenu.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.papylhomme.assistantaidomenu.db.DBHelper;
import org.papylhomme.assistantaidomenu.provider.requests.GetProductRequest;
import org.papylhomme.assistantaidomenu.provider.requests.GetStockItemRequest;
import org.papylhomme.assistantaidomenu.provider.requests.ListProductGroupRequest;
import org.papylhomme.assistantaidomenu.provider.requests.ListProductRequest;
import org.papylhomme.assistantaidomenu.provider.requests.ListStockItemRequest;
import org.papylhomme.assistantaidomenu.provider.requests.StockUpdateRequest;
import org.papylhomme.assistantaidomenu.provider.requests.ListUpdatedStockItemRequest;
import org.papylhomme.assistantaidomenu.provider.requests.MergeStockUpdateRequest;
import org.papylhomme.assistantaidomenu.provider.requests.NewStockItemRequest;

import java.util.ArrayList;



/**
 * A provider to access the database
 *
 * TODO handle timeout when db is locked
 */
public class DataProvider extends ContentProvider {

    /**
     * Provider authority
     */
    public static final String AUTHORITY = "org.papylhomme.assistantaidomenu.provider";


    /**
     * A list of handlers
     */
    private ArrayList<ContentRequestHandler> handlers = new ArrayList<>();



    /**
     * Handle onCreate to instantiate a database helper and initialize content handlers
     *
     * @return True
     */
    @Override
    public boolean onCreate() {
        Context context = getContext();
        DBHelper dbHelper = new DBHelper(context);

        addHandler(new GetProductRequest(context, dbHelper));
        addHandler(new ListProductRequest(context, dbHelper));

        addHandler(new NewStockItemRequest(context, dbHelper));
        addHandler(new GetStockItemRequest(context, dbHelper));
        addHandler(new ListStockItemRequest(context, dbHelper));
        addHandler(new ListUpdatedStockItemRequest(context, dbHelper));
        addHandler(new MergeStockUpdateRequest(context, dbHelper));

        addHandler(new StockUpdateRequest(context, dbHelper));

        addHandler(new ListProductGroupRequest(context, dbHelper));

        return true;
    }



    /**
     * Query the database
     *
     * @param uri An Uri for the query
     * @param projection A projection for the select request
     * @param selection A selection for the select request
     * @param args Argument for the selection
     * @param sortOrder A sort order
     * @return A cursor instance
     */
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] args, String sortOrder) {
        ContentRequestHandler handler = getHandler(uri);

        //sanity check
        if(handler == null)
            return null;

        Cursor cursor = handler.query(uri, projection, selection, args, sortOrder);
        if(cursor != null)
            cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }



    /**
     * Get a mime type for the request
     *
     * @param uri A request Uri
     * @return A mime type
     */
    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        ContentRequestHandler handler = getHandler(uri);

        //sanity check
        if(handler == null)
            return null;

        return handler.getType(uri);
    }



    /**
     * Insert content into the database
     *
     * @param uri An Uri for insertion
     * @param contentValues Values to insert
     * @return The number of affecter rows
     */
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues contentValues) {
        ContentRequestHandler handler = getHandler(uri);

        //sanity check
        if(handler == null)
            return null;

        return handler.insert(uri, contentValues);
    }



    /**
     * Delete content from the database
     *
     * @param uri An Uri for deletion
     * @param selection Selection for the delete request
     * @param args Arguments for the selection
     * @return The number of affected rows
     */
    @Override
    public int delete(@NonNull Uri uri, String selection, String[] args) {
        ContentRequestHandler handler = getHandler(uri);

        //sanity check
        if(handler == null)
            return 0;

        return handler.delete(uri, selection, args);
    }



    /**
     * Update content in the database
     *
     * @param uri An Uri for update
     * @param contentValues Values to update
     * @param selection Selection for the updated request
     * @param args Arguments for the selection
     * @return The number of affected rows
     */
    @Override
    public int update(@NonNull Uri uri, ContentValues contentValues, String selection, String[] args) {
        ContentRequestHandler handler = getHandler(uri);

        //sanity check
        if(handler == null)
            return 0;

        return handler.update(uri, contentValues, selection, args);
    }



    /**
     * Register a content request handler
     *
     * @param handler The handler to register
     */
    private void addHandler(ContentRequestHandler handler) {
        handlers.add(handler);
    }



    /**
     * Retrieve an handler to process the given uri
     *
     * @param uri An uri to process
     * @return An handler instance
     */
    private ContentRequestHandler getHandler(Uri uri) {
        for(ContentRequestHandler handler : handlers)
            if(handler.match(uri))
                return handler;

        return null;
    }

}
