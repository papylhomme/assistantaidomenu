package org.papylhomme.assistantaidomenu.provider.requests;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import org.papylhomme.assistantaidomenu.db.DBHelper;
import org.papylhomme.assistantaidomenu.db.Query;
import org.papylhomme.assistantaidomenu.provider.ContentRequestHandler;

import org.papylhomme.assistantaidomenu.db.DBContract.Products;
import org.papylhomme.assistantaidomenu.db.DBContract.ProductGroup;

import static org.papylhomme.assistantaidomenu.provider.DataProvider.AUTHORITY;


/**
 * Base class for products requests
 */
@SuppressWarnings("WeakerAccess")
public abstract class BaseProductsRequest extends ContentRequestHandler {


    /**
     * The Uri for products
     */
    public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + Products.TABLE_NAME);



    /**
     * Constructor
     *
     * @param context  A context instance
     * @param dbHelper A DBHelper instance
     */
    protected BaseProductsRequest(Context context, DBHelper dbHelper) {
        super(context, dbHelper);
    }



    /**
     * Query a product
     *
     * @param uri A request uri
     * @param projection Projection for the query
     * @param selection Selection for the query
     * @param args Arguments for selection
     * @param sortOrder A sort order
     * @return A cursor wrapping the result
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] args, String sortOrder) {
        return new Query(Products.TABLE_NAME, projection)
                .join(Products.TABLE_NAME, Products.COLUMN_GROUP_ID, ProductGroup.TABLE_NAME, ProductGroup._ID)
                .selection(selection, args)
                .sort(sortOrder)
                .execute(dbHelper.getReadableDatabase());
    }



    /**
     * Get the default projection for products
     *
     * @return A projection list
     */
    protected static String[] getDefaultProjection() {
        return new String[] {
                Query.fqcn2n(Products.TABLE_NAME, Products._ID),
                Query.fqcn2n(Products.TABLE_NAME, Products.COLUMN_NAME),
                Query.fqcn2n(Products.TABLE_NAME, Products.COLUMN_UNIT),
                Query.fqcn2n(ProductGroup.TABLE_NAME, ProductGroup.COLUMN_NAME),
                Query.fqcn2n(Products.TABLE_NAME, Products.COLUMN_BRAND),
        };
    }
}
