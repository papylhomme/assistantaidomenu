package org.papylhomme.assistantaidomenu.ui;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.papylhomme.assistantaidomenu.service.ImportService;
import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.db.DBContract;
import org.papylhomme.assistantaidomenu.tools.DBActivity;
import org.papylhomme.assistantaidomenu.ui.products.ActivityProducts;
import org.papylhomme.assistantaidomenu.ui.stock.ActivityShortBBD;
import org.papylhomme.assistantaidomenu.ui.stock.ActivityStock;
import org.papylhomme.assistantaidomenu.ui.stock.ActivityMergeStockUpdates;

import java.util.Date;


/**
 * Home context
 */
public class ActivityHome extends DBActivity {


    /**
     * Request code for products import
     */
    private static final int REQUEST_IMPORT_PRODUCTS = 1;


    /**
     * Request code for stock import
     */
    private static final int REQUEST_IMPORT_STOCK = 2;


    /**
     * A database connection instance
     */
    private SQLiteDatabase db;


    /**
     * The import products progress bar
     */
    private ProgressBar progressBarImportProducts;



    /**
     * The import stock progress bar
     */
    private ProgressBar progressBarImportStock;



    /**
     * Callback used to retrieve an context layout
     *
     * @return A layout resource
     */
    @Override
    protected int getContentView() {
        return R.layout.activity_home;
    }



    /**
     * Handle onCreate to setup buttons
     *
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = dbHelper.getWritableDatabase();

        //setup shortcuts to activities
        setShortcutListener(findViewById(R.id.card_products), ActivityProducts.class);
        setShortcutListener(findViewById(R.id.card_stock), ActivityStock.class);
        setShortcutListener(findViewById(R.id.card_sync), ActivityMergeStockUpdates.class);
        setShortcutListener(findViewById(R.id.card_short_bbd), ActivityShortBBD.class);

        //setup buttons
        setImportHandler(findViewById(R.id.button_import_products), REQUEST_IMPORT_PRODUCTS);
        setImportHandler(findViewById(R.id.button_import_stock), REQUEST_IMPORT_STOCK);

        //hide the progress bars
        progressBarImportProducts = (ProgressBar) findViewById(R.id.progressBar_import_products);
        progressBarImportProducts.setVisibility(View.GONE);

        progressBarImportStock = (ProgressBar) findViewById(R.id.progressBar_import_stock);
        progressBarImportStock.setVisibility(View.GONE);

        //setup and refresh labels
        updateDetails(new Integer[]{0, 0, 0, 0});

        IntentFilter eventMatcher = new IntentFilter();
        eventMatcher.addAction(ImportService.INTENT_ACTION_EVENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String originalAction = intent.getStringExtra(ImportService.PARAM_ORIGINAL_ACTION);

                if(ImportService.ACTION_IMPORT_PRODUCTS.equals(originalAction))
                    handleImportEvent(intent, progressBarImportProducts, R.string.sb_imported_products);

                else if(ImportService.ACTION_IMPORT_STOCK.equals(originalAction))
                    handleImportEvent(intent, progressBarImportStock, R.string.sb_imported_stock_items);
            }
        }, eventMatcher);
    }



    /**
     * Handle onResume by restarting the fetch details task
     */
    @Override
    protected void onResume() {
        super.onResume();
        new FetchDetailsTask().execute();
    }



    /**
     * Update the details labels
     *
     * @param details The details
     */
    private void updateDetails(Integer[] details) {
        ((TextView) findViewById(R.id.label_products_in_catalog)).setText(getString(R.string.label_products_in_catalog, details[0]));
        ((TextView) findViewById(R.id.label_products_in_stock)).setText(getString(R.string.label_products_in_stock, details[1]));
        ((TextView) findViewById(R.id.label_waiting_stock_update)).setText(getString(R.string.label_waiting_stock_update, details[2]));
        ((TextView) findViewById(R.id.label_short_bbd_items)).setText(getString(R.string.label_short_bbd_items, details[3]));
    }



    /**
     * Helper to set handlers for import buttons
     *
     * @param view The clickable view
     * @param request The request code
     */
    private void setImportHandler(View view, final int request) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("text/csv");

                startActivityForResult(intent, request);
            }
        });
    }



    /**
     * Handle results from import requests
     *
     * @param requestCode The request code
     * @param resultCode The result code
     * @param data Data from the import request
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data != null && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();

            switch(requestCode) {
                case REQUEST_IMPORT_PRODUCTS: ImportService.importProducts(this, uri); break;
                case REQUEST_IMPORT_STOCK: ImportService.importStock(this, uri); break;
            }
        }
    }



    /**
     * Handle import events
     *
     * @param intent The event
     * @param progressBar The related progress bar
     * @param message A string resource for the message
     */
    private void handleImportEvent(Intent intent, ProgressBar progressBar, int message) {
       switch ((ImportService.EVENT_TYPE) intent.getSerializableExtra(ImportService.PARAM_EVENT_TYPE)) {
            case IMPORT_PROGRESS:
                if(progressBar.getVisibility() != View.VISIBLE)
                    progressBar.setVisibility(View.VISIBLE);

                progressBar.setProgress(intent.getIntExtra(ImportService.PARAM_IMPORT_PROGRESS, 0));
                break;

            case IMPORT_FINISHED:
                progressBar.setVisibility(View.GONE);

                Snackbar.make(findViewById(R.id.coordinator_layout),
                        getString(message, intent.getIntExtra(ImportService.PARAM_IMPORT_COUNT, 0)),
                        Snackbar.LENGTH_SHORT).show();

                new FetchDetailsTask().execute();

                break;

           case IMPORT_ERROR:
                progressBar.setVisibility(View.GONE);

                Snackbar.make(findViewById(R.id.coordinator_layout),
                        intent.getStringExtra(ImportService.PARAM_ERROR_MESSAGE),
                        Snackbar.LENGTH_LONG).show();

                break;

        }
    }



    /**
     * A task to fetch details about the database tables
     */
    class FetchDetailsTask extends AsyncTask<Void, Void, Integer[]> {

        /**
         * Process queries in background
         *
         * @param voids No parameters
         * @return The details from the database
         */
        @Override
        protected Integer[] doInBackground(Void... voids) {
            Cursor c;
            Integer[] results = new Integer[4];

            c = db.rawQuery("SELECT COUNT(*) FROM " + DBContract.Products.TABLE_NAME, null);
            c.moveToFirst();
            results[0] = c.getInt(0);
            c.close();

            c = db.rawQuery("SELECT COUNT(*) FROM " + DBContract.Stock.TABLE_NAME, null);
            c.moveToFirst();
            results[1] = c.getInt(0);
            c.close();

            c = db.rawQuery("SELECT COUNT(*) FROM " + DBContract.StockUpdate.TABLE_NAME, null);
            c.moveToFirst();
            results[2] = c.getInt(0);
            c.close();

            String limitDate = Long.toString(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 7).getTime());
            c = db.rawQuery("SELECT COUNT(*) FROM " + DBContract.Stock.TABLE_NAME
                    + " WHERE " + DBContract.Stock.COLUMN_BBD + " < " + limitDate
                    , null);
            c.moveToFirst();
            results[3] = c.getInt(0);
            c.close();

            return results;
        }



        /**
         * Handle details from the database
         *
         * @param integers The details
         */
        @Override
        protected void onPostExecute(Integer[] integers) {
            updateDetails(integers);
        }
    }
}
