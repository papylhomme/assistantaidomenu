package org.papylhomme.assistantaidomenu.ui.stock;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.db.DBContract.Stock;
import org.papylhomme.assistantaidomenu.db.DBStockItem;
import org.papylhomme.assistantaidomenu.db.Query;
import org.papylhomme.assistantaidomenu.provider.requests.ListStockItemRequest;
import org.papylhomme.assistantaidomenu.provider.requests.StockUpdateRequest;
import org.papylhomme.assistantaidomenu.tools.CursorListFragment;
import org.papylhomme.assistantaidomenu.tools.DrawerActivity;

import java.util.ArrayList;



/**
 * An activity to display the stock
 */
public class ActivityStock
        extends DrawerActivity
        implements ListFragmentStockItems.OnStockItemSelectedListener,
            ListFragmentStockItems.StockItemsFiltersListener,
            DialogStockUpdate.OnNewStockUpdateListener,
            LoaderManager.LoaderCallbacks<Cursor> {


    /**
     * Tag for vertical list fragment
     */
    protected static final String TAG_FRAGMENT_LIST = "simple_list";


    /**
     * Tag for lateral list fragment (pager)
     */
    protected static final String TAG_FRAGMENT_LATERAL_LIST = "lateral_list";


    /**
     * Current cursor instance
     */
    private Cursor currentCursor;



    /**
     * Callback used to retrieve an context layout
     *
     * @return A layout resource
     */
    @Override
    protected int getContentView() {
        return R.layout.activity_stock;
    }



    /**
     * Override to open a connection to the database
     *
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //install recycler list fragment
        if(savedInstanceState == null) {
            Fragment fragment = getListFragment();
            fragment.setArguments(getIntent().getExtras());
            getFragmentManager().beginTransaction()
                    .add(R.id.content_frame, fragment, TAG_FRAGMENT_LIST)
                    .commit();

            //execute the transaction in order to initialize the fragment before the loader's creation
            getFragmentManager().executePendingTransactions();
        }


        //initialize the loader
        getLoaderManager().initLoader(0, null, this);
    }



    /**
     * Handle stock item selected by displaying a FragmentStockItem
     *
     * @param position The current position of the selected item
     * @param stockItem The selected stock item
     */
    @Override
    public void onStockItemSelected(int position, DBStockItem stockItem) {
        ListFragmentStockItems list = (ListFragmentStockItems) getFragmentManager().findFragmentByTag(TAG_FRAGMENT_LIST);

        ListFragmentStockItemsLateralPager fragment = new ListFragmentStockItemsLateralPager();
        fragment.setArguments(ListFragmentStockItemsLateralPager.parameters(position, list.displayEmptyStock(), list.getSearchQuery(), list.getGroupFilter()));

        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment, TAG_FRAGMENT_LATERAL_LIST)
                .addToBackStack(null)
                .commit();

        getFragmentManager().executePendingTransactions();
        fragment.updateItems(currentCursor);
    }



    /**
     * Helper to instantiate the main stock item list fragment.
     *
     * @return A fragment to display a list of stock items
     */
    protected ListFragmentStockItems getListFragment() {
        return new ListFragmentStockItems();
    }



    /**
     * Create a loader to list the stock items
     *
     * @param i Id of the loader to create
     * @param params Parameters for the loader
     * @return A loader instance
     */
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle params) {
        return ListStockItemRequest.list(this, getDefaultSelection());
    }



    /**
     * Handle loader finish
     *
     * @param loader The finished loader
     * @param cursor A cursor of results
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        this.currentCursor = cursor;

        updateStockItems(cursor);
    }



    /**
     * Handle loader reset
     *
     * @param loader The reset loader
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        this.currentCursor = null;

        if(!isFinishing())
            updateStockItems(null);
    }



    /**
     * Update the list fragments with a new list of stock items
     *
     * @param cursor A cursor wrapping the stock items
     */
    private void updateStockItems(Cursor cursor) {
        CursorListFragment f;

        f = (CursorListFragment) getFragmentManager().findFragmentByTag(TAG_FRAGMENT_LIST);
        if(f != null) f.updateItems(cursor);

        f = (CursorListFragment) getFragmentManager().findFragmentByTag(TAG_FRAGMENT_LATERAL_LIST);
        if(f != null) f.updateItems(cursor);
    }



    /**
     * Update the search query for the list
     *
     * @param query A search query
     */
    @Override
    public void updateSearchQuery(String query) {
        //restart the loader
        getLoaderManager().restartLoader(0, null, this);
    }



    /**
     * Update the groups filter for the list
     *
     * @param groups An array of groups to select for the list
     */
    @Override
    public void updateGroupsFilter(ArrayList<Integer> groups) {
        //restart the loader
        getLoaderManager().restartLoader(0, null, this);
    }



    /**
     * Set the empty stock filter
     *
     * @param displayEmptyStock Whether to display empty stock or not
     */
    @Override
    public void setDisplayEmptyStock(boolean displayEmptyStock) {
        //restart the loader
        getLoaderManager().restartLoader(0, null, this);
    }



    /**
     * Retrieve an instance of selection configured with default filters
     */
    protected Query.Selection getDefaultSelection() {
        ListFragmentStockItems f = (ListFragmentStockItems) getFragmentManager().findFragmentByTag(TAG_FRAGMENT_LIST);

        Query.Selection selection = new Query.Selection();

        if(!f.displayEmptyStock()) selection.condition(Query.fqcn(Stock.TABLE_NAME, Stock.COLUMN_CURRENT), ">", "0");

        selection.glob(Query.fqcn(Stock.TABLE_NAME, Stock.COLUMN_NORMALIZED_NAME), f.getSearchQuery())
                .in(Query.fqcn(Stock.TABLE_NAME, Stock.COLUMN_GROUP_ID), f.getGroupFilter());

        return selection;
    }



    /**
     * Handle a new stock update
     *
     * @param uri       Uri of the new stock update
     * @param type      Type of update
     * @param operation Operation (+/-/=)
     * @param amount    The amount of the update
     */
    @Override
    public void onNewStockUpdate(final Uri uri, String type, String operation, double amount) {
        Snackbar.make(findViewById(R.id.coordinator_layout), String.format("%s : %s %.2f", type, operation, amount), Snackbar.LENGTH_LONG)
                        .setAction("Annuler", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                StockUpdateRequest.removeStockUpdate(ActivityStock.this, Long.parseLong(uri.getLastPathSegment()));
                            }
                        })
                        .show();
    }
}
