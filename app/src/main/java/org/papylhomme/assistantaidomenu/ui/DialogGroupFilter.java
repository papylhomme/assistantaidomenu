package org.papylhomme.assistantaidomenu.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Pair;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.db.DBContract;
import org.papylhomme.assistantaidomenu.provider.requests.ListProductGroupRequest;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A dialog allowing to filter items by group
 */
public class DialogGroupFilter extends DialogFragment implements LoaderManager.LoaderCallbacks<Cursor> {


    /**
     * Parameter for relation table
     */
    public static final String PARAM_RELATION_TABLE = "RELATION_TABLE";


    /**
     * Parameter name to store the group filter
     */
    public static final String PARAM_GROUPS_FILTER = "GROUP_FILTER";


    /**
     * The root view of the dialog
     */
    private View rootView;


    /**
     * An adapter instance
     */
    private ProductGroupsAdapter adapter;



    /**
     * Handle onCreateDialog to instantiate the dialog
     *
     * @param savedInstanceState A saved instance
     * @return A dialog instance
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        createView();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //build the dialog
        builder.setTitle(R.string.title_group_filter)
                .setIcon(R.drawable.icon_filter_dark)
                .setView(rootView)
                .setPositiveButton(R.string.label_validate, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        OnGroupFilterChangeListener target = (OnGroupFilterChangeListener) getTargetFragment();
                        target.onGroupFilterChange(adapter.getFilters());
                    }
                })
                .setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

        getLoaderManager().restartLoader(0, null, this);

        return builder.create();
    }




    /**
     * Inflate and initialize the view
     */
    private void createView() {
        //inflate the layout
        rootView = getActivity().getLayoutInflater().inflate(R.layout.dialog_group_filter, null);
        ListView listView = (ListView) rootView.findViewById(R.id.list_product_groups);

        adapter = new ProductGroupsAdapter(getActivity());
        listView.setAdapter(adapter);

        rootView.findViewById(R.id.label_select_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_VIEW_SETTING);
                adapter.selectAll();
            }
        });

        rootView.findViewById(R.id.label_select_none).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_VIEW_SETTING);
                adapter.selectNone();
            }
        });

        rootView.findViewById(R.id.label_select_invert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_VIEW_SETTING);
                adapter.selectInvert();
            }
        });
    }



    /**
     * Create a loader to fetch the idsByNames
     *
     * @param i The loader if
     * @param bundle Parameters for the loader
     * @return A loader instance
     */
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        if(getArguments() != null && getArguments().containsKey(PARAM_RELATION_TABLE))
            return ListProductGroupRequest.createProductGroupListLoader(getActivity(), getArguments().getString(PARAM_RELATION_TABLE));
        else
            return ListProductGroupRequest.createProductGroupListLoader(getActivity());
    }



    /**
     * Handle loader's results
     *
     * @param loader A loader instance
     * @param cursor A cursor instance
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        ArrayList<Pair<String, Integer>> groups = new ArrayList<>();
        while(cursor.moveToNext()) {
            int id = (int) cursor.getLong(cursor.getColumnIndex(DBContract.ProductGroup._ID));
            final String name = cursor.getString(cursor.getColumnIndex(DBContract.ProductGroup.COLUMN_NAME));

            groups.add(new Pair<>(name, id));
        }

        adapter.setGroups(groups);
    }



    /**
     * Handle onLoaderReset by resetting the view
     *
     * @param loader The loader instance
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.setGroups(null);
    }



    /**
     * A list adapter for product's group
     */
    class ProductGroupsAdapter extends BaseAdapter {


        /**
         * A map to store the selection
         */
        private HashMap<String, Boolean> checkedGroups = new HashMap<>();


        /**
         * An array list of groups
         */
        private ArrayList<Pair<String, Integer>> groups;


        /**
         * Constructor
         *
         * @param context A context instance
         */
        ProductGroupsAdapter(Context context) {
            super();
        }



        /**
         * Select all the idsByNames
         */
        void selectAll() {
            if(groups == null)
                return;

            checkedGroups.clear();
            notifyDataSetChanged();
        }



        /**
         * Unselect all the idsByNames
         */
        void selectNone() {
            if(groups == null)
                return;

            for(Pair<String, Integer> p : groups)
                checkedGroups.put(p.first, false);

            notifyDataSetChanged();
        }



        /**
         * Invert the selection
         */
        void selectInvert() {
            if(groups == null)
                return;

            for(Pair<String, Integer> p : groups)
                checkedGroups.put(p.first, !isChecked(p.first));

            notifyDataSetChanged();
        }



        /**
         * Test whether a group is checked or not
         *
         * @param name The group name
         * @return True if the group is checked
         */
        private boolean isChecked(String name) {
            return checkedGroups.containsKey(name) ? checkedGroups.get(name) : true;
        }



        /**
         * Retrieve the number of items in the adapter
         *
         * @return The number of items
         */
        @Override
        public int getCount() {
            return groups != null ? groups.size() : 0;
        }



        /**
         * Get the item for the given position
         *
         * @param i A position
         * @return An item
         */
        @Override
        public Object getItem(int i) {
            return this.groups.get(i);
        }



        /**
         * Get the item id
         *
         * @param i The position
         * @return An item id
         */
        @Override
        public long getItemId(int i) {
            return this.groups.get(i).second;
        }



        /**
         * Create or rebind a view for the given item
         *
         * @param i The position
         * @param view An existing view or null
         * @param viewGroup A view group
         * @return A binded view
         */
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if(view == null)
                view = getActivity().getLayoutInflater().inflate(android.R.layout.simple_list_item_checked, null);

            final String name = this.groups.get(i).first;

            final CheckedTextView textView = (CheckedTextView) view.findViewById(android.R.id.text1);
            textView.setText(name);
            textView.setChecked(isChecked(name));

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_VIEW_SETTING);
                    checkedGroups.put(name, !textView.isChecked());
                    textView.setChecked(isChecked(name));
                }
            });

            return view;
        }



        /**
         * Update the list of idsByNames
         *
         * @param groups A map of idsByNames and their id
         */
        void setGroups(ArrayList<Pair<String, Integer>> groups) {
            this.groups = groups;
            this.checkedGroups.clear();

            //early exit if no content
            if(this.groups == null) {
                notifyDataSetChanged();
                return;
            }

            if(getArguments() != null && getArguments().containsKey(PARAM_GROUPS_FILTER)) {
                ArrayList<Integer> groupFilter = getArguments().getIntegerArrayList(PARAM_GROUPS_FILTER);

                if(groupFilter != null && !groupFilter.isEmpty())
                    for(Pair<String, Integer> p : groups)
                        this.checkedGroups.put(p.first, groupFilter.contains(p.second));
            }

            notifyDataSetChanged();
        }



        /**
         * Get a selection to filter the selected groups
         *
         * @return A selection string
         */
        ArrayList<Integer> getFilters() {
            if(checkedGroups.isEmpty())
                return null;

            ArrayList<Integer> res = new ArrayList<>();
            for(Pair<String, Integer> p : groups) {
                if(isChecked(p.first)) {
                    res.add(p.second);
                }
            }

            return res;
        }
    }



    /**
     * Interface to handle group filter change
     */
    public interface OnGroupFilterChangeListener {

        /**
         * Handle a group filter change
         *
         * @param groupFilter The new group filter
         */
        void onGroupFilterChange(ArrayList<Integer> groupFilter);

    }
}
