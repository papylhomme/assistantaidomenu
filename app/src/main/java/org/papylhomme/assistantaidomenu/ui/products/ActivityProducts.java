package org.papylhomme.assistantaidomenu.ui.products;


import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.db.DBProduct;
import org.papylhomme.assistantaidomenu.provider.requests.ListProductRequest;
import org.papylhomme.assistantaidomenu.tools.CursorListFragment;
import org.papylhomme.assistantaidomenu.tools.DrawerActivity;
import org.papylhomme.assistantaidomenu.tools.SearchableRecyclerListFragment;

import java.util.ArrayList;


/**
 * Activity displaying the products
 *
 * Main fragment is a searchable recycler list, leading to a
 * lateral pager displaying product fragments on item click
 */
public class ActivityProducts
        extends DrawerActivity
        implements ListFragmentProducts.OnProductSelectedListener,
            SearchableRecyclerListFragment.SearchableListFiltersListener,
            LoaderManager.LoaderCallbacks<Cursor> {


    /**
     * Current cursor instance
     */
    private Cursor currentCursor;



    /**
     * Callback used to retrieve an context layout
     *
     * @return A layout resource
     */
    @Override
    protected int getContentView() {
        return R.layout.activity_products;
    }



    /**
     * Override to open a connection to the database
     *
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //install recycler list fragment
        if(savedInstanceState == null) {
            Fragment fragment = new ListFragmentProducts();
            fragment.setArguments(getIntent().getExtras());

            getFragmentManager().beginTransaction()
                    .add(R.id.content_frame, fragment, ListFragmentProducts.class.getName())
                    .commit();

            //execute the transaction in order to initialize the fragment before the loader's creation
            getFragmentManager().executePendingTransactions();
        }


        //initialize the loader
        getLoaderManager().initLoader(0, null, this);
    }



    /**
     * Handle product selected by displaying a ListFragmentProductsLateralPager
     *
     * @param position Position of the item in the list
     * @param product The selected product
     */
    @Override
    public void onProductSelected(int position, DBProduct product) {
        ListFragmentProducts productsList = (ListFragmentProducts) getFragmentManager().findFragmentByTag(ListFragmentProducts.class.getName());

        ListFragmentProductsLateralPager fragment = new ListFragmentProductsLateralPager();
        fragment.setArguments(ListFragmentProductsLateralPager.parameters(productsList.getSearchQuery(), productsList.getGroupFilter(), position));

        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment, ListFragmentProductsLateralPager.class.getName())
                .addToBackStack(null)
                .commit();

        getFragmentManager().executePendingTransactions();
        fragment.updateItems(currentCursor);
    }



    /**
     * Create a loader to list the products
     *
     * @param i Id of the loader to create
     * @param params Parameters for the loader
     * @return A loader instance
     */
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle params) {
        ListFragmentProducts f = (ListFragmentProducts) getFragmentManager().findFragmentByTag(ListFragmentProducts.class.getName());
        return ListProductRequest.list(this, f.getSearchQuery(), f.getGroupFilter());
    }



    /**
     * Handle loader finish
     *
     * @param loader The finished loader
     * @param cursor A cursor of results
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        this.currentCursor = cursor;

        updateProducts(cursor);
    }



    /**
     * Handle loader reset
     *
     * @param loader The reset loader
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        this.currentCursor = null;

        if(!isFinishing())
            updateProducts(null);
    }



    /**
     * Update the list fragments with a new list of products
     *
     * @param cursor A cursor wrapping the products
     */
    private void updateProducts(Cursor cursor) {
        CursorListFragment f;

        f = (CursorListFragment) getFragmentManager().findFragmentByTag(ListFragmentProducts.class.getName());
        if(f != null) f.updateItems(cursor);

        f = (CursorListFragment) getFragmentManager().findFragmentByTag(ListFragmentProductsLateralPager.class.getName());
        if(f != null) f.updateItems(cursor);
    }



    /**
     * Update the search query for the list
     *
     * @param query A search query
     */
    @Override
    public void updateSearchQuery(String query) {
        //restart the loader
        getLoaderManager().restartLoader(0, null, this);
    }



    /**
     * Update the groups filter for the list
     *
     * @param groups An array of groups to select for the list
     */
    @Override
    public void updateGroupsFilter(ArrayList<Integer> groups) {
        //restart the loader
        getLoaderManager().restartLoader(0, null, this);
    }
}
