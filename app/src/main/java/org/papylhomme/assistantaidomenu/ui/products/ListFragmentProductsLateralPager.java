package org.papylhomme.assistantaidomenu.ui.products;

import android.app.Fragment;
import android.os.Bundle;

import org.papylhomme.assistantaidomenu.tools.SiblingsListFragment;

import java.util.ArrayList;



/**
 * A lateral pager for Products
 */
public class ListFragmentProductsLateralPager extends SiblingsListFragment {

    /**
     * Create a new FragmentProduct for the given product id
     *
     * @param id A product id for the new fragment
     * @return A fragment instance
     */
    @Override
    protected Fragment createSiblingFragment(int id) {
        FragmentProduct fragmentProduct = new FragmentProduct();
        fragmentProduct.setArguments(FragmentProduct.parameters(id));

        return fragmentProduct;
    }



    /**
     * Create a bundle of parameters for a FragmentProduct instance
     *
     * @return A bundle instance
     */
    public static Bundle parameters(String searchQuery, ArrayList<Integer> groupFilter, int position) {
        Bundle args = new Bundle();
        args.putInt(PARAM_POSITION, position);
        args.putString(PARAM_SEARCH_QUERY, searchQuery);
        args.putIntegerArrayList(PARAM_GROUP_FILTER, groupFilter);

        return args;
    }
}
