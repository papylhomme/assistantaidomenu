package org.papylhomme.assistantaidomenu.ui.stock;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.provider.requests.ListUpdatedStockItemRequest;
import org.papylhomme.assistantaidomenu.tools.CoordinatorLayoutActivity;



/**
 * An context to display updated stock items.
 *
 * Top level is a standard list of stock items, second level is a lateral list, with a FAB
 * to merge the stock updates
 */
public class ActivityMergeStockUpdates extends ActivityStock
        implements LoaderManager.LoaderCallbacks<Cursor>,
        CoordinatorLayoutActivity.FABController {


    /**
     * Instantiate the fragment to display the list of updated stock items
     *
     * @return An updated stock items list fragment
     */
    @Override
    protected ListFragmentStockItems getListFragment() {
        return new ListFragmentUpdatedStockItems();
    }



    /**
     *
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uninstallFABController();

        // handle fragment updates by updating the FAB visibility
        final FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Fragment fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_LATERAL_LIST);

                if(fragment != null && fragment.isVisible())
                    installFABController(ActivityMergeStockUpdates.this);
                else
                    uninstallFABController();
            }
        });
    }



    /**
     * Create a loader to fetch the number of updated stock items
     *
     * @param i Loader id
     * @param bundle Parameters for the loader
     * @return A loader instance
     */
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        FragmentManager fragmentManager = getFragmentManager();
        ListFragmentUpdatedStockItems fragment = (ListFragmentUpdatedStockItems) fragmentManager.findFragmentByTag(TAG_FRAGMENT_LIST);

        //sanity check
        if(fragment == null)
            return null;

        return ListUpdatedStockItemRequest.list(this, getDefaultSelection());
    }



    /**
     * Handle loader finish by returning to the main list when there is no more
     * stock item with stock updates
     *
     * @param loader A loader instance
     * @param cursor A cursor instance
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if(cursor.getCount() == 0 && getFragmentManager().getBackStackEntryCount() == 1)
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    getFragmentManager().popBackStack();
                }
            });
        else
            super.onLoadFinished(loader, cursor);
    }



    /**
     * Retrieve an image resource for the FAB
     *
     * @return An image resource
     */
    @Override
    public int getFABImageResource() {
        return R.drawable.icon_merge_light;
    }



    /**
     * Handle FAB clicked
     *
     * @param view The clicked view
     */
    @Override
    public void onFABClickListener(View view) {
        ListFragmentStockItemsLateralPager fragment = (ListFragmentStockItemsLateralPager) getFragmentManager().findFragmentByTag(TAG_FRAGMENT_LATERAL_LIST);

        if(fragment != null) {
            int stockItemId = fragment.getCurrentItemId();

            if (stockItemId >= 0) {
                DialogConfirmMergeStockUpdate.confirmMerge(ActivityMergeStockUpdates.this, stockItemId);
            }
        }
    }
}
