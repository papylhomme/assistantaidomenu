package org.papylhomme.assistantaidomenu.ui.stock;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.futuremind.recyclerviewfastscroll.SectionTitleProvider;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.db.DBContract;
import org.papylhomme.assistantaidomenu.db.DBStockItem;
import org.papylhomme.assistantaidomenu.provider.requests.NewStockItemRequest;
import org.papylhomme.assistantaidomenu.tools.CursorRecyclerAdapter;
import org.papylhomme.assistantaidomenu.tools.CursorViewHolder;
import org.papylhomme.assistantaidomenu.tools.RecyclerSwipeHelper;
import org.papylhomme.assistantaidomenu.tools.SearchableRecyclerListFragment;
import org.papylhomme.assistantaidomenu.ui.DialogGroupFilter;
import org.papylhomme.assistantaidomenu.ui.products.ActivityPickProduct;


/**
 * A fragment displaying a list of stock items
 */
public class ListFragmentStockItems
        extends SearchableRecyclerListFragment<DBStockItem, ListFragmentStockItems.StockViewHolder> {


    /**
     * Parameter to filter empty stocks
     */
    protected static final String PARAM_DISPLAY_EMPTY_STOCK = "FILTER_EMPTY_STOCK";


    /**
     * An instance of OnStockItemSelectedListener to handle stock item selection
     */
    private OnStockItemSelectedListener stockItemSelectedListener;



    /**
     * Get the layout resource to inflate for the fragment
     *
     * @return A layout resource
     */
    @Override
    protected int getLayout() {
        return R.layout.fragment_stock_list;
    }



    /**
     * Get the id of the recycler view in the fragment's layout
     *
     * @return An id for the recycler view
     */
    @Override
    protected int getRecyclerView() {
        return R.id.list_stock_items;
    }



    /**
     * Create an instance of adapter for the recycler view
     *
     * @return An adapter instance
     */
    @Override
    protected CursorRecyclerAdapter<DBStockItem, StockViewHolder> createListAdapter() {
        return new StockRecyclerAdapter();
    }



    /**
     * Test whether to filter empty stock or not
     *
     * @return True to filter empty stock, false otherwise
     */
    public boolean displayEmptyStock() {
        return getArguments().getBoolean(PARAM_DISPLAY_EMPTY_STOCK, false);
    }



    /**
     * Override to setup the swipe handler to the list
     *
     * @param inflater A layout inflater instance
     * @param container A view container instance
     * @param savedInstanceState A saved instance
     * @return The root view of the fragment
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        //setup swipe helper
        ItemTouchHelper touchHelper = RecyclerSwipeHelper.setupRecyclerView(getActivity(), list);
        touchHelper.attachToRecyclerView(list);

        return rootView;
    }

    /**
     * Handle onCreateOptionsMenu to install the stock item menu
     *
     * @param menu An instance of menu
     * @param inflater An instance of menu inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_list_stock_items, menu);
    }



    /**
     * Handle onPrepareOptionsMenu by synchronizing the display empty stock item state
     *
     * @param menu The menu instance
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.menu_item_display_empty_stock).setChecked(displayEmptyStock());
    }



    /**
     * Override onOptionsItemSelected to handle empty stock items filtering
     *
     * @param item The selected menu item
     * @return True if the click was handled
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_item_display_empty_stock) {
            getArguments().putBoolean(PARAM_DISPLAY_EMPTY_STOCK, !item.isChecked());
            if(searchableListFiltersListener instanceof StockItemsFiltersListener)
                ((StockItemsFiltersListener) searchableListFiltersListener).setDisplayEmptyStock(displayEmptyStock());

            return true;

        } else if(item.getItemId() == R.id.menu_item_new_stock_item) {
            startActivityForResult(new Intent(getActivity(), ActivityPickProduct.class), 0);
            return true;

        } else
            return super.onOptionsItemSelected(item);
    }



    /**
     * Override to restrict the groups to the one used by the stock
     *
     * @return A bundle
     */
    @Override
    protected Bundle getGroupFilterArguments() {
        Bundle args = new Bundle();
        args.putString(DialogGroupFilter.PARAM_RELATION_TABLE, DBContract.Stock.TABLE_NAME);
        return args;
    }



    /**
     * Handle onAttach to setup the stock item selected listener
     *
     * @param context The parent context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        stockItemSelectedListener = (OnStockItemSelectedListener) context;
    }



    /**
     * Handle onDetach to reset the stock item selected listener
     */
    @Override
    public void onDetach() {
        super.onDetach();
        stockItemSelectedListener = null;
    }



    /**
     * Handle results from child activity
     *
     * @param requestCode The original request code
     * @param resultCode The result code
     * @param data An intent containing the results
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 0 && resultCode == Activity.RESULT_OK) {
            int productId = data.getIntExtra(ActivityPickProduct.PARAM_PRODUCT_ID, -1);

            if(productId >= 0)
                NewStockItemRequest.insertNewStockItem(getActivity(), productId);
        }
    }



    /**
     * A recycler adapter for stock items
     */
    class StockRecyclerAdapter extends CursorRecyclerAdapter<DBStockItem, StockViewHolder>
            implements SectionTitleProvider,
            RecyclerSwipeHelper.RecyclerSwipeAdapter {

        /**
         * Constructor
         */
        StockRecyclerAdapter() {
            super(R.layout.list_item_stock_item);
        }


        /**
         * Create a new view holder
         *
         * @param view A view created with the layout resource
         * @return A view holder instance
         */
        @Override
        protected StockViewHolder createViewHolder(View view) {
            return new StockViewHolder(view);
        }



        /**
         * Create an instance of DBStockItem from the cursor
         *
         * @param cursor An instance of cursor
         * @return An instance of DBStockItem
         */
        @Override
        protected DBStockItem createItem(Cursor cursor) {
            return new DBStockItem(cursor);
        }



        /**
         * Get the section name for the given position
         *
         * @param position Position of the row in adapter
         * @return The text to be shown in the bubble
         */
        @Override
        public String getSectionTitle(int position) {
            return getItem(position).getName().substring(0, 1);
        }



        /**
         * Test if the item at position is swipeable
         *
         * @param position Position of the item
         * @return True if the item is swipeable, false otherwise
         */
        @Override
        public boolean isItemSwipeable(int position) {
            return true;
        }



        /**
         * Swipe the item at position
         *
         * @param position Position of the item
         */
        @Override
        public void onSwipeItem(int position) {
            DBStockItem stockItem = adapter.getItem(position);

            if(stockItem != null) {
                DialogStockUpdate dialog = new DialogStockUpdate();
                dialog.setArguments(DialogStockUpdate.parameters(stockItem));
                dialog.show(getFragmentManager(), "stock_update");
            }

            //revert the row to its original position
            notifyItemChanged(position);
        }
    }



    /**
     * View holder for stock items
     */
    class StockViewHolder extends CursorViewHolder<DBStockItem> {

        /**
         * The text view for the item name
         */
        private final TextView name;


        /**
         * The text view for the group
         */
        private final TextView group;


        /**
         * The text view for the brand
         */
        private final TextView brand;


        /**
         * The text view for the item unit
         */
        private final TextView unit;


        /**
         * The text view for the current amount of the item
         */
        private final TextView current;


        /**
         * The text view for the best before date
         */
        private final TextView bbd;


        /**
         * An instance of stock item
         */
        private DBStockItem stockItem;



        /**
         * Constructor
         *
         * @param itemView An instance of item view
         */
        StockViewHolder(View itemView) {
            super(itemView);

            this.name = (TextView) itemView.findViewById(R.id.label_stock_item_name);
            this.group = (TextView) itemView.findViewById(R.id.label_stock_item_group);
            this.brand = (TextView) itemView.findViewById(R.id.label_stock_item_brand);
            this.unit = (TextView) itemView.findViewById(R.id.label_stock_item_unit);
            this.bbd = (TextView) itemView.findViewById(R.id.label_stock_item_bbd);
            this.current = (TextView) itemView.findViewById(R.id.label_stock_item_current);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(stockItem != null)
                        stockItemSelectedListener.onStockItemSelected(getAdapterPosition(), stockItem);
                }
            });

            current.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(stockItem != null) {
                        DialogStockUpdate dialog = new DialogStockUpdate();
                        dialog.setArguments(DialogStockUpdate.parameters(stockItem));
                        dialog.show(getFragmentManager(), "stock_update");
                    }
                }
            });
        }



        /**
         * Bind the view to the given stock item
         *
         * @param stockItem An instance of stock item
         */
        @SuppressLint("DefaultLocale")
        public void bind(DBStockItem stockItem) {
            this.stockItem = stockItem;

            CardViewStockItem cardViewStockItem = (CardViewStockItem) this.itemView;
            cardViewStockItem.setStockItem(this.stockItem);

            this.name.setText(stockItem.getName());
            this.group.setText(stockItem.getGroup());
            this.brand.setText(stockItem.getBrand());
            this.unit.setText(stockItem.getUnit());
            this.bbd.setText(stockItem.getFormattedBbd());
            this.current.setText(String.format("%.2f", stockItem.getCurrentQuantity()));
        }
    }



    /**
     * Interface to handle stock item selection
     */
    public interface OnStockItemSelectedListener {

        /**
         * Handle selection of the given stock item
         *
         * @param position The position of the stock item in the list
         * @param stockItem A stock item instance
         */
        void onStockItemSelected(int position, DBStockItem stockItem);
    }



    /**
     * Describe the interface for the activity list filters
     */
    public interface StockItemsFiltersListener extends SearchableListFiltersListener {


        /**
         * Set the empty stock filter
         *
         * @param displayEmptyStock Whether to display empty stock or not
         */
        void setDisplayEmptyStock(boolean displayEmptyStock);

    }
}
