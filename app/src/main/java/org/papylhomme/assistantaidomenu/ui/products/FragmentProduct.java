package org.papylhomme.assistantaidomenu.ui.products;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.db.DBProduct;
import org.papylhomme.assistantaidomenu.provider.requests.GetProductRequest;



/**
 * A fragment to display a product
 */
public class FragmentProduct extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {


    /**
     * Parameter for product id
     */
    private static String PARAM_PRODUCT_ID = "PRODUCT_ID";


    /**
     * Instance of the product to display
     */
    private DBProduct product;



    /**
     * Handle onResumt by restarting the loader
     */
    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().restartLoader(0, null, this);
    }



    /**
     * Create and initialize the view
     *
     * @param inflater A layout inflater instance
     * @param container A view container
     * @param savedInstanceState A saved instance
     * @return A view for the fragment
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_product, container, false);

        updateView(rootView);

        return rootView;
    }



    /**
     * Update the view content using the current product instance
     *
     * @param rootView The root view to update
     */
    private void updateView(View rootView) {
        //sanity check
        if(rootView == null)
            return;

        TextView name = (TextView) rootView.findViewById(R.id.label_product_name);
        TextView brand = (TextView) rootView.findViewById(R.id.label_product_brand);
        TextView family = (TextView) rootView.findViewById(R.id.label_product_family);

        String unknown = getString(android.R.string.unknownName);

        if(product == null) {
            name.setText(unknown);
            brand.setText(unknown);
            family.setText(unknown);
        } else {
            name.setText(product.getName());
            brand.setText(product.getBrand());
            family.setText(product.getGroup());
        }

    }



    /**
     * Create a loader to fetch the product
     *
     * @param i The loader if
     * @param bundle Parameters for the loader
     * @return A loader instance
     */
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        int id = -1;
        Bundle params = getArguments();
        if(params.containsKey(PARAM_PRODUCT_ID))
            id = getArguments().getInt(PARAM_PRODUCT_ID);

        return GetProductRequest.get(getActivity(), id);
    }



    /**
     * Handle loader results
     *
     * @param loader The loader instance
     * @param cursor A cursor instance
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if(cursor.moveToFirst()) {
            product = new DBProduct(cursor);
        } else {
            product = null;
        }

        updateView(getView());
    }



    /**
     * Handle loader reset by resetting the view
     *
     * @param loader A loader instance
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        product = null;
        updateView(getView());
    }



    /**
     * Create a bundle of parameters for a FragmentProduct instance
     *
     * @param product The product to pass as parameters
     * @return A bundle instance
     */
    public static Bundle parameters(DBProduct product) {
        Bundle args = new Bundle();
        args.putInt(PARAM_PRODUCT_ID, product.getId());
        return args;
    }



    /**
     * Create a bundle of parameters for a FragmentProduct instance
     *
     * @param id The product's id
     * @return A bundle instance
     */
    public static Bundle parameters(int id) {
        Bundle args = new Bundle();
        args.putInt(PARAM_PRODUCT_ID, id);
        return args;
    }
}
