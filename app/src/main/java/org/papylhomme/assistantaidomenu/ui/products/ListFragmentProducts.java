package org.papylhomme.assistantaidomenu.ui.products;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.TextView;

import com.futuremind.recyclerviewfastscroll.SectionTitleProvider;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.db.DBProduct;
import org.papylhomme.assistantaidomenu.tools.CursorRecyclerAdapter;
import org.papylhomme.assistantaidomenu.tools.CursorViewHolder;
import org.papylhomme.assistantaidomenu.tools.SearchableRecyclerListFragment;



/**
 * A fragment displaying a list of products
 */
public class ListFragmentProducts extends SearchableRecyclerListFragment<DBProduct, ListFragmentProducts.ProductViewHolder> {


    /**
     * An instance of OnProductSelectedListener to handle product selection
     */
    private OnProductSelectedListener productSelectedListener;



    /**
     * Get the layout resource to inflate for the fragment
     *
     * @return A layout resource
     */
    @Override
    protected int getLayout() {
        return R.layout.fragment_products_list;
    }



    /**
     * Get the id of the recycler view in the fragment's layout
     *
     * @return An id for the recycler view
     */
    @Override
    protected int getRecyclerView() {
        return R.id.list_products;
    }



    /**
     * Create an instance of adapter for the recycler view
     *
     * @return An adapter instance
     */
    @Override
    protected CursorRecyclerAdapter<DBProduct, ProductViewHolder> createListAdapter() {
        return new ProductRecyclerAdapter();
    }



    /**
     * Handle onAttach to setup the product selected listener
     *
     * @param context The parent context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        productSelectedListener = (OnProductSelectedListener) context;
    }



    /**
     * Handle onDetach to reset the product selected listener
     */
    @Override
    public void onDetach() {
        super.onDetach();
        productSelectedListener = null;
    }



    /**
     * A recycler adapter for products
     */
    class ProductRecyclerAdapter extends CursorRecyclerAdapter<DBProduct, ProductViewHolder> implements SectionTitleProvider {


        /**
         * Constructor
         */
        ProductRecyclerAdapter() {
            super(R.layout.list_item_product);
        }



        /**
         * Create a new view holder
         *
         * @param view A view created with the layout resource
         * @return A view holder instance
         */
        @Override
        protected ProductViewHolder createViewHolder(View view) {
            return new ProductViewHolder(view);
        }



        /**
         * Create an instance of DBProduct from the cursor
         *
         * @param cursor An instance of cursor
         * @return An instance of DBProduct
         */
        @Override
        protected DBProduct createItem(Cursor cursor) {
            return new DBProduct(cursor);
        }



        /**
         * Get the section name for the given position
         *
         * @param position Position of the row in adapter
         * @return The text to be shown in the bubble
         */
        @Override
        public String getSectionTitle(int position) {
            return getItem(position).getName().substring(0, 1);
        }
    }



    /**
     * View holder for products
     */
    class ProductViewHolder extends CursorViewHolder<DBProduct> {

        /**
         * The text view for the product name
         */
        private final TextView name;


        /**
         * The text view for the product brand
         */
        private final TextView brand;


        /**
         * The text view for the product family
         */
        private final TextView family;


        /**
         * An instance of product
         */
        private DBProduct product;



        /**
         * Constructor
         *
         * @param itemView An instance of item view
         */
        ProductViewHolder(View itemView) {
            super(itemView);

            this.name = (TextView) itemView.findViewById(R.id.label_product_name);
            this.brand = (TextView) itemView.findViewById(R.id.label_product_brand);
            this.family = (TextView) itemView.findViewById(R.id.label_product_family);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(product != null)
                        productSelectedListener.onProductSelected(getAdapterPosition(), product);
                }
            });
        }



        /**
         * Bind the view to the given product
         *
         * @param product A product instance
         */
        public void bind(DBProduct product) {
            this.product = product;

            this.name.setText(product.getName());
            this.brand.setText(product.getBrand());
            this.family.setText(product.getGroup());
        }
    }



    /**
     * Interface to handle product selection
     */
    public interface OnProductSelectedListener {

        /**
         * Handle selection of the given product
         *
         * @param position The position of the item in the list
         * @param product A product instance
         */
        void onProductSelected(int position, DBProduct product);
    }
}
