package org.papylhomme.assistantaidomenu.ui.stock;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.papylhomme.assistantaidomenu.R;



/**
 * A fragment displaying a list of updated stock items
 */
public class ListFragmentUpdatedStockItems extends ListFragmentStockItems {


    /**
     * Overridden to display empty stock items by default
     *
     * @param savedInstanceState A saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getArguments().putBoolean(PARAM_DISPLAY_EMPTY_STOCK, true);
    }



    /**
     * Overridden to hide the unused menu items of ListFragmentStockItems
     *
     * @param menu An instance of menu
     * @param inflater An instance of menu inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem i = menu.findItem(R.id.menu_item_display_empty_stock);
        if(i != null) i.setVisible(false);

        i = menu.findItem(R.id.menu_item_new_stock_item);
        if(i != null) i.setVisible(false);
    }
}
