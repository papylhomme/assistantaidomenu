package org.papylhomme.assistantaidomenu.ui.products;

import android.app.Activity;
import android.content.Intent;

import org.papylhomme.assistantaidomenu.db.DBProduct;



/**
 * An activity to pick a product from a product's list
 */
public class ActivityPickProduct extends ActivityProducts {


    /**
     * Parameter for the product id
     */
    public static final String PARAM_PRODUCT_ID = "PRODUCT_ID";



    /**
     * Handle selection of the given product
     *
     * @param position Position of the selected item
     * @param product A product instance
     */
    @Override
    public void onProductSelected(int position, DBProduct product) {
        Intent res = new Intent();
        res.putExtra(PARAM_PRODUCT_ID, product.getId());

        setResult(Activity.RESULT_OK, res);
        finish();
    }


}
