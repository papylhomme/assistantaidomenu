package org.papylhomme.assistantaidomenu.ui.stock;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.papylhomme.assistantaidomenu.R;
import org.papylhomme.assistantaidomenu.db.DBStockItem;
import org.papylhomme.assistantaidomenu.provider.requests.GetStockItemRequest;
import org.papylhomme.assistantaidomenu.provider.requests.StockUpdateRequest;
import org.papylhomme.assistantaidomenu.tools.SquareButton;


/**
 * A dialog for stock update
 */
public class DialogStockUpdate extends DialogFragment implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener {

    /**
     * Parameter for the stock item id
     */
    private static final String PARAM_STOCK_ITEM_ID = "STOCK_ITEM_ID";


    /**
     * Instance of the listener for new stock update
     */
    private OnNewStockUpdateListener onNewStockUpdateListener;


    /**
     * An instance of stock item
     */
    private DBStockItem stockItem;


    /**
     * The text edit for the amount
     */
    private EditText edit;


    /**
     * A spinner for the type of operation (labels)
     */
    private Spinner spinnerType;


    /**
     * A spinner for the update operation (+, -, =)
     */
    private Spinner spinnerOperation;


    /**
     * The root view of the dialog
     */
    private View rootView;


    /**
     * Buffers for the calculator view
     */
    private String buffer1;
    private String operation;
    private String buffer2;



    /**
     * Handle onCreateDialog to instantiate the dialog
     *
     * @param savedInstanceState A saved instance
     * @return A dialog instance
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        createView();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //build the dialog
        builder.setTitle(R.string.title_update_stock)
                .setIcon(R.drawable.icon_stock_update_dark)
                .setView(rootView)
                .setPositiveButton(R.string.label_validate, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        updateStock();
                    }
                })
                .setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

        getLoaderManager().restartLoader(0, null, this);

        return builder.create();
    }



    /**
     * Inflate and initialize the view
     */
    private void createView() {
        //inflate the layout
        rootView = getActivity().getLayoutInflater().inflate(R.layout.dialog_stock_update, null);
        edit = (EditText) rootView.findViewById(R.id.edit_stock_update_amount);
        spinnerType = (Spinner) rootView.findViewById(R.id.spinner_stock_update_type);
        spinnerOperation = (Spinner) rootView.findViewById(R.id.spinner_stock_update_operation);


        //initialize auto-selection operation from type
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_VIEW_SETTING);

                //TODO do something about this, change in resources can brake the behavior
                switch(i) {
                    case 0:
                    case 1:
                    case 2: spinnerOperation.setSelection(0); break;
                    case 3: spinnerOperation.setSelection(2); break;
                    case 4: spinnerOperation.setSelection(1); break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //nothing to do
            }
        });

        //haptic feedback when switching operation
        spinnerOperation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_VIEW_SETTING);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //nothing to do
            }
        });


        //initialize calculator buttons
        TableLayout table = ((TableLayout) rootView.findViewById(R.id.calculator_view));
        for(int i = 0; i < table.getChildCount(); i++) {
            View child = table.getChildAt(i);

            if (child instanceof TableRow) {
                TableRow row = (TableRow) child;
                for (int j = 0; j < row.getChildCount(); j++) {
                    View button = row.getChildAt(j);

                    if (button instanceof SquareButton)
                        button.setOnClickListener(DialogStockUpdate.this);

                }
            }
        }


        //set link to add current amount to calculator
        rootView.findViewById(R.id.label_stock_item_current).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(buffer1 == null)
                    buffer1 = Double.toString(stockItem.getCurrentQuantity());
                else if(operation != null)
                    buffer2 = Double.toString(stockItem.getCurrentQuantity());

                edit.setText(getCurrentOperation());
            }
        });
    }



    /**
     * Override to register the activity as OnNewStockUpdateListener
     *
     * @param context A context instance
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof OnNewStockUpdateListener)
            this.onNewStockUpdateListener = (OnNewStockUpdateListener) context;
        else
            this.onNewStockUpdateListener = null;
    }



    /**
     * Override to unregister the OnNewStockUpdateListener
     */
    @Override
    public void onDetach() {
        super.onDetach();
        this.onNewStockUpdateListener = null;
    }



    /**
     * Update the view with the current instance of stock item
     */
    private void updateView() {
        //sanity check
        if(this.rootView == null)
            return;

        TextView name = ((TextView) this.rootView.findViewById(R.id.label_stock_item_name));
        TextView current = ((TextView) this.rootView.findViewById(R.id.label_stock_item_current));
        TextView unit = ((TextView) this.rootView.findViewById(R.id.label_stock_item_unit));

        String unknown = getString(android.R.string.unknownName);

        if(stockItem == null) {
            name.setText(unknown);
            current.setText(unknown);
            unit.setText(unknown);
        } else {
            name.setText(stockItem.getName());
            current.setText(String.format("%.2f", stockItem.getCurrentQuantity()));
            unit.setText(stockItem.getUnit());
        }
    }



    /**
     * Validate the operation and update the stock
     */
    private void updateStock() {
        try {
            execute();

            if(buffer1 != null) {
                double amount = Double.parseDouble(buffer1);
                String type = (String) spinnerType.getSelectedItem();
                String operation = (String) spinnerOperation.getSelectedItem();

                final Uri newStockUpdate = StockUpdateRequest.newStockUpdate(getActivity(), stockItem.getId(), amount, type, operation);

                if(newStockUpdate != null && onNewStockUpdateListener != null)
                    onNewStockUpdateListener.onNewStockUpdate(newStockUpdate, type, operation, amount);
            }
        } catch(Exception ex) {
            Log.e(DialogStockUpdate.class.getName(), "Error calculating value", ex);
        }
    }




    /**
     * Create a loader to fetch information on the stock item
     *
     * @param i The loader id
     * @param bundle Parameters for the loader
     * @return A loader instance
     */
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        int id = getArguments().getInt(PARAM_STOCK_ITEM_ID, -1);
        return GetStockItemRequest.get(getActivity(), id);
    }



    /**
     * Handle data from the loader by updating the view
     *
     * @param loader The loader instance
     * @param cursor A cursor for the results
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if(cursor.moveToFirst()) {
            stockItem = new DBStockItem(cursor);
        } else {
            stockItem = null;
        }

        updateView();
    }



    /**
     * Handle onLoaderReset by resetting the view
     *
     * @param loader The loader instance
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        stockItem = null;
        updateView();
    }



    /**
     * Generate parameters for the dialog
     *
     * @param stockItem A stock item instance
     * @return A bundled stock item
     */
    public static Bundle parameters(DBStockItem stockItem) {
        Bundle args = new Bundle();
        args.putInt(PARAM_STOCK_ITEM_ID, stockItem.getId());

        return args;
    }



    /**
     * Handle calculator click
     *
     * @param view The clicked button
     */
    @Override
    public void onClick(View view) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY, HapticFeedbackConstants.FLAG_IGNORE_VIEW_SETTING);
        String button = ((SquareButton) view).getText().toString();

        if("C".equals(button)) {
            buffer1 = null;
            buffer2 = null;
            operation = null;

        } else if("DEL".equals(button) && buffer2 != null) {
            if(buffer2.length() > 1) buffer2 = buffer2.substring(0, buffer2.length() - 1);
            else buffer2 = null;

        } else if("DEL".equals(button) && operation != null) {
            operation = null;

        } else if("DEL".equals(button) && buffer1 != null) {
            if (buffer1.length() > 1) buffer1 = buffer1.substring(0, buffer1.length() - 1);
            else buffer1 = null;

        } else if(",".equals(button) && buffer2 != null) {
            if(!buffer2.contains(".")) buffer2 = buffer2 + ".";

        } else if(",".equals(button) && operation == null && buffer1 != null) {
            if(!buffer1.contains(".")) buffer1 = buffer1 + ".";

        } else if(button.matches("[0-9]") && operation == null) {
            buffer1 = buffer1 == null ? button : buffer1 + button;

        } else if(button.matches("[0-9]") && operation != null) {
            buffer2 = buffer2 == null ? button : buffer2 + button;

        } else if(button.matches("[+x/-]") && buffer2 != null) {
            execute();
            operation = button;

        } else if(button.matches("[+x/-]") && buffer1 != null) {
            operation = button;

        } else if("=".equals(button) && buffer2 != null) {
            execute();
        }


        edit.setText(getCurrentOperation());
    }



    /**
     * Execute a calculator operation
     */
    private void execute() {
        if(buffer1 == null || buffer2 == null || operation == null)
            return;

        try {
            float value1 = Float.parseFloat(buffer1);
            float value2 = Float.parseFloat(buffer2);

            if("+".equals(operation))
                buffer1 = Float.toString(value1 + value2);
            else if("-".equals(operation))
                buffer1 = Float.toString(value1 - value2);
            else if("x".equals(operation))
                buffer1 = Float.toString(value1 * value2);
            else if("/".equals(operation))
                buffer1 = Float.toString(value1 / value2);

        } catch(Exception ex) {
            buffer1 = null;
        } finally {
            buffer2 = null;
            operation = null;
        }
    }



    /**
     * Retrieve a string representation of the current operation
     *
     * @return A string
     */
    private String getCurrentOperation() {
        StringBuilder builder = new StringBuilder();

        if(buffer1 == null)
            return "";

        builder.append(buffer1);

        if(operation != null)
            builder.append(" ").append(operation);

        if(buffer2 != null)
            builder.append(" ").append(buffer2);

        return builder.toString();
    }



    /**
     * Describe the interface for onStckUpdateListener
     */
    public interface OnNewStockUpdateListener {


        /**
         * Handle a new stock update
         *
         * @param uri Uri of the new stock update
         * @param type Type of update
         * @param operation Operation (+/-/=)
         * @param amount The amount of the update
         */
        void onNewStockUpdate(Uri uri, String type, String operation, double amount);

    }

}
